'use strict';

class Task {
  constructor(title, description){
    this.title = title;
    this.description = description;
  }
}

//
// For Browser
//
export default Task;

//
// For Node
//
// module.exports = Task;
