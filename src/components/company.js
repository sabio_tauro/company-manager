'use strict';
//
// For Node
//
// let utils = require('../utils/utils');
// let Task = require('./task');
// let Employee = require('./employee');

//
// For Browser
//
import utils from '../utils/utils';
import Employee from './employee';
import Task from './task';

class Company {
  constructor(title){
    this.title = title;
    this.staff = [];
    this.companyTasks = [];
  }

  _hireCandidate(candidate){
    if(candidate instanceof Employee){
      this.staff.push(candidate);

      console.log(`Hooray! You just hired ${candidate.fullName}`);
    } else{
      console.group();
      console.error('Candidate does not fit, because he is not an Employee');
      console.dir(candidate);
      console.groupEnd();
    }
  }

  hire(candidates){
    candidates = utils.checkArray(candidates);

    candidates.forEach(candidate => this._hireCandidate(candidate));
  }

  _fireEmployee(employee){
    if(employee instanceof Employee){
      employee._clearTasks();

      let employeeIndex = this.staff.indexOf(employee);
      this.staff.splice(employeeIndex, 1);

      console.log(`You just fired that lazy bastard: ${employee.fullName}`);
    } else{
      console.group();
      console.error('You tried to fire not your employee. Take a rest!');
      console.dir(employee);
      console.groupEnd();
    }
  }

  fire(employees){
    employees = utils.checkArray(employees);

    employees.forEach(employee => this._fireEmployee(employee));
  }

  createTask(title, description){
    let newTask = new Task(title, description);

    this.companyTasks.push(newTask);
    console.log('Task created!', newTask.title, newTask.description);

    return newTask;
  }

  assignTask(task, assignedEmployees){
    if(task instanceof Task){
      assignedEmployees = utils.checkArray(assignedEmployees);

      assignedEmployees.forEach(({assignedTasks}) => {
        assignedTasks.push(task);
      });
    } else{
      console.group();
      console.error('It is not a Task you try to give');
      console.dir(task);
      console.groupEnd();
    }
  }

  cancelTask(task, assignedEmployees){
    if(task instanceof Task){
      assignedEmployees = utils.checkArray(assignedEmployees);

      assignedEmployees.forEach(({assignedTasks}) => {
        let taskIndex = assignedTasks.indexOf(task);
        assignedTasks.splice(taskIndex, 1);
      });
    } else{
      console.group();
      console.error('It is not a Task you try to cancel');
      console.dir(task);
      console.groupEnd();
    }
  }
}

//
// For Browser
//
export default Company;

//
// For Node
//
// module.exports = Company;
