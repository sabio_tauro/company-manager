'use strict';

class Employee {
  constructor({firstName, lastName, speciality}) {
    if(!firstName || !lastName){
      throw new Error('You need to provide at least firstName and lastName of the employee');
    }

    this.firstName = firstName;
    this.lastName = lastName;
    this.speciality = speciality;
    this.assignedTasks = [];
    this.completedTasks = [];
  }

  get fullName(){
    return this.firstName + ' ' + this.lastName;
  }

  _clearTasks(){
    this.assignedTasks = [];
  }

  _removeFromAssigned(task){
    let taskIndex = this.assignedTasks.indexOf(task);

    return this.assignedTasks.splice(taskIndex, 1)[0];
  }

  completeTask(task){
    let completedTask = this._removeFromAssigned(task);

    this.completedTasks.push(completedTask);

    console.log(`Great! ${this.fullName} complete task: ${task.title}`);
  }

  declineTask(task){
    this._removeFromAssigned(task);

    console.log(`:( ${this.fullName} decline task: ${task.title}`);
  }

  takeVacation(){
    console.log('Yey! Vacation!!!');
  }
}

//
// For Browser
//
export default Employee;

//
// For Node
//
// module.exports = Employee;
