const utils = {
  checkArray(arrayToCheck){
    if(!Array.isArray(arrayToCheck)){
      return [arrayToCheck];
    }

    return arrayToCheck;
  }
}

//
// For Browser
//
export default utils;

//
// For Node
//
// module.exports = utils;
