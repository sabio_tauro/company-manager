'use strict';
//
// For Node
//
// let Task = require('./components/task');
// let Company = require('./components/company');
// let Employee = require('./components/employee');

//
// For Browser
//
import Task from './components/task';
import Company from './components/company';
import Employee from './components/employee';

let testVar = 'just for test';

// Create Specialists
let frontend = new Employee({
  firstName: 'Bruce',
  lastName: 'Wayne',
  speciality: 'FrontEnd'
});
console.log('New Employee: ', frontend);

let backend = new Employee({
  firstName: 'Doctor',
  lastName: 'Manhattan',
  speciality: 'BackEnd'
});
console.log('New Employee: ', backend);

let tester = new Employee({
  firstName: 'Jessica',
  lastName: 'Jones',
  speciality: 'Tester'
});
console.log('New Employee: ', tester);

let designer = new Employee({
  firstName: 'Jean',
  lastName: 'Grey'
});
console.log('New Employee: ', designer);

// Create Company
let greatCompany = new Company('Great Company');
console.log('Created Company: ', greatCompany);

// Hire some people
greatCompany.hire(frontend);
greatCompany.hire(testVar);
greatCompany.hire([backend, designer, tester]);
console.log('Company hired some people', greatCompany);

// Create Task
let newImportantTask = greatCompany.createTask('Very Important Task', 'Your mission, if you will to accept it...');
console.log('Company created tasks', greatCompany);

// Assign employees to the task
greatCompany.assignTask(newImportantTask, designer);
greatCompany.assignTask(testVar, frontend);
greatCompany.assignTask(newImportantTask, [backend, frontend, tester]);
console.log('Company assign tasks', [backend, frontend, tester, designer]);

// Some employee decline task
designer.declineTask(newImportantTask);
console.log('Let\'s see what Employee Object looks now: ', designer);

// And gets fired for that
greatCompany.fire(designer);
greatCompany.fire(testVar);
console.log('We fired designer!', greatCompany);

// Company decides that task do not need tester
greatCompany.cancelTask(testVar, tester);
greatCompany.cancelTask(newImportantTask, tester);
console.log('Tester', tester);

// And he go to vacation
tester.takeVacation();

// Developers finishes task
[frontend, backend].forEach(employee => employee.completeTask(newImportantTask));
console.log('Guys completed task', [backend, frontend]);

// And company don't need them no more
greatCompany.fire([backend, frontend, tester])
console.log('There is no staff in Company', greatCompany);

