/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Task = function Task(title, description) {
  _classCallCheck(this, Task);

  this.title = title;
  this.description = description;
};

//
// For Browser
//


exports.default = Task;

//
// For Node
//
// module.exports = Task;

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Employee = function () {
  function Employee(_ref) {
    var firstName = _ref.firstName,
        lastName = _ref.lastName,
        speciality = _ref.speciality;

    _classCallCheck(this, Employee);

    if (!firstName || !lastName) {
      throw new Error('You need to provide at least firstName and lastName of the employee');
    }

    this.firstName = firstName;
    this.lastName = lastName;
    this.speciality = speciality;
    this.assignedTasks = [];
    this.completedTasks = [];
  }

  _createClass(Employee, [{
    key: '_clearTasks',
    value: function _clearTasks() {
      this.assignedTasks = [];
    }
  }, {
    key: '_removeFromAssigned',
    value: function _removeFromAssigned(task) {
      var taskIndex = this.assignedTasks.indexOf(task);

      return this.assignedTasks.splice(taskIndex, 1)[0];
    }
  }, {
    key: 'completeTask',
    value: function completeTask(task) {
      var completedTask = this._removeFromAssigned(task);

      this.completedTasks.push(completedTask);

      console.log('Great! ' + this.fullName + ' complete task: ' + task.title);
    }
  }, {
    key: 'declineTask',
    value: function declineTask(task) {
      this._removeFromAssigned(task);

      console.log(':( ' + this.fullName + ' decline task: ' + task.title);
    }
  }, {
    key: 'takeVacation',
    value: function takeVacation() {
      console.log('Yey! Vacation!!!');
    }
  }, {
    key: 'fullName',
    get: function get() {
      return this.firstName + ' ' + this.lastName;
    }
  }]);

  return Employee;
}();

//
// For Browser
//


exports.default = Employee;

//
// For Node
//
// module.exports = Employee;

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

//
// For Node
//
// let Task = require('./components/task');
// let Company = require('./components/company');
// let Employee = require('./components/employee');

//
// For Browser
//

var _task = __webpack_require__(0);

var _task2 = _interopRequireDefault(_task);

var _company = __webpack_require__(3);

var _company2 = _interopRequireDefault(_company);

var _employee = __webpack_require__(1);

var _employee2 = _interopRequireDefault(_employee);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var testVar = 'just for test';

// Create Specialists
var frontend = new _employee2.default({
  firstName: 'Bruce',
  lastName: 'Wayne',
  speciality: 'FrontEnd'
});
console.log('New Employee: ', frontend);

var backend = new _employee2.default({
  firstName: 'Doctor',
  lastName: 'Manhattan',
  speciality: 'BackEnd'
});
console.log('New Employee: ', backend);

var tester = new _employee2.default({
  firstName: 'Jessica',
  lastName: 'Jones',
  speciality: 'Tester'
});
console.log('New Employee: ', tester);

var designer = new _employee2.default({
  firstName: 'Jean',
  lastName: 'Grey'
});
console.log('New Employee: ', designer);

// Create Company
var greatCompany = new _company2.default('Great Company');
console.log('Created Company: ', greatCompany);

// Hire some people
greatCompany.hire(frontend);
greatCompany.hire(testVar);
greatCompany.hire([backend, designer, tester]);
console.log('Company hired some people', greatCompany);

// Create Task
var newImportantTask = greatCompany.createTask('Very Important Task', 'Your mission, if you will to accept it...');
console.log('Company created tasks', greatCompany);

// Assign employees to the task
greatCompany.assignTask(newImportantTask, designer);
greatCompany.assignTask(testVar, frontend);
greatCompany.assignTask(newImportantTask, [backend, frontend, tester]);
console.log('Company assign tasks', [backend, frontend, tester, designer]);

// Some employee decline task
designer.declineTask(newImportantTask);
console.log('Let\'s see what Employee Object looks now: ', designer);

// And gets fired for that
greatCompany.fire(designer);
greatCompany.fire(testVar);
console.log('We fired designer!', greatCompany);

// Company decides that task do not need tester
greatCompany.cancelTask(testVar, tester);
greatCompany.cancelTask(newImportantTask, tester);
console.log('Tester', tester);

// And he go to vacation
tester.takeVacation();

// Developers finishes task
[frontend, backend].forEach(function (employee) {
  return employee.completeTask(newImportantTask);
});
console.log('Guys completed task', [backend, frontend]);

// And company don't need them no more
greatCompany.fire([backend, frontend, tester]);
console.log('There is no staff in Company', greatCompany);

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

//
// For Node
//
// let utils = require('../utils/utils');
// let Task = require('./task');
// let Employee = require('./employee');

//
// For Browser
//

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _utils = __webpack_require__(4);

var _utils2 = _interopRequireDefault(_utils);

var _employee = __webpack_require__(1);

var _employee2 = _interopRequireDefault(_employee);

var _task = __webpack_require__(0);

var _task2 = _interopRequireDefault(_task);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Company = function () {
  function Company(title) {
    _classCallCheck(this, Company);

    this.title = title;
    this.staff = [];
    this.companyTasks = [];
  }

  _createClass(Company, [{
    key: '_hireCandidate',
    value: function _hireCandidate(candidate) {
      if (candidate instanceof _employee2.default) {
        this.staff.push(candidate);

        console.log('Hooray! You just hired ' + candidate.fullName);
      } else {
        console.group();
        console.error('Candidate does not fit, because he is not an Employee');
        console.dir(candidate);
        console.groupEnd();
      }
    }
  }, {
    key: 'hire',
    value: function hire(candidates) {
      var _this = this;

      candidates = _utils2.default.checkArray(candidates);

      candidates.forEach(function (candidate) {
        return _this._hireCandidate(candidate);
      });
    }
  }, {
    key: '_fireEmployee',
    value: function _fireEmployee(employee) {
      if (employee instanceof _employee2.default) {
        employee._clearTasks();

        var employeeIndex = this.staff.indexOf(employee);
        this.staff.splice(employeeIndex, 1);

        console.log('You just fired that lazy bastard: ' + employee.fullName);
      } else {
        console.group();
        console.error('You tried to fire not your employee. Take a rest!');
        console.dir(employee);
        console.groupEnd();
      }
    }
  }, {
    key: 'fire',
    value: function fire(employees) {
      var _this2 = this;

      employees = _utils2.default.checkArray(employees);

      employees.forEach(function (employee) {
        return _this2._fireEmployee(employee);
      });
    }
  }, {
    key: 'createTask',
    value: function createTask(title, description) {
      var newTask = new _task2.default(title, description);

      this.companyTasks.push(newTask);
      console.log('Task created!', newTask.title, newTask.description);

      return newTask;
    }
  }, {
    key: 'assignTask',
    value: function assignTask(task, assignedEmployees) {
      if (task instanceof _task2.default) {
        assignedEmployees = _utils2.default.checkArray(assignedEmployees);

        assignedEmployees.forEach(function (_ref) {
          var assignedTasks = _ref.assignedTasks;

          assignedTasks.push(task);
        });
      } else {
        console.group();
        console.error('It is not a Task you try to give');
        console.dir(task);
        console.groupEnd();
      }
    }
  }, {
    key: 'cancelTask',
    value: function cancelTask(task, assignedEmployees) {
      if (task instanceof _task2.default) {
        assignedEmployees = _utils2.default.checkArray(assignedEmployees);

        assignedEmployees.forEach(function (_ref2) {
          var assignedTasks = _ref2.assignedTasks;

          var taskIndex = assignedTasks.indexOf(task);
          assignedTasks.splice(taskIndex, 1);
        });
      } else {
        console.group();
        console.error('It is not a Task you try to cancel');
        console.dir(task);
        console.groupEnd();
      }
    }
  }]);

  return Company;
}();

//
// For Browser
//


exports.default = Company;

//
// For Node
//
// module.exports = Company;

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
var utils = {
  checkArray: function checkArray(arrayToCheck) {
    if (!Array.isArray(arrayToCheck)) {
      return [arrayToCheck];
    }

    return arrayToCheck;
  }
};

//
// For Browser
//
exports.default = utils;

//
// For Node
//
// module.exports = utils;

/***/ })
/******/ ]);